﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class CellularAutomata : MonoBehaviour
{
    public Pawn[] pawns;
    
    public static CellularAutomata sInstance = null;
    public int width;
    public int height;

    public Vector2 tileSize;

    public GameObject tile;
    public GameObject wall;
    
    public string seed;
    public bool useRandomSeed;

    [Range(0, 100)] public int randomFillPercent;
    [Range(0, 10)] public int smooth;

    private int[,] _map;
    private int[,] _cost;
    private int[,] _smoothCost;
    private int[,] _smoothMap;
    private Tile[,] _currentMap;
    private List<Vector2Int> _playableArea = new List<Vector2Int>();

    [SerializeField] private EventGenerator eventGenerator;

    private void Awake()
    {
        if (sInstance)
        {
            Destroy(this);
        }
        else
        {
            sInstance = this;
        }
    }

    void Start()
    {
        GenerateMap();
    }

    public void SetSmooth(int smooth)
    {
        this.smooth = smooth;
    }

    public void SetFillPercentage(int fillPercentage)
    {
        randomFillPercent = fillPercentage;
    }
    
    public void SetRandomSeed(bool useRandomSeed)
    {
        this.useRandomSeed = useRandomSeed;
    }

    public void SetSeed(string seed)
    {
        this.seed = seed;
    }
    
    public Tile[,] GetCurrentMap()
    {
        return _currentMap;
    }

    public List<Tile> GetNeighboursOf(Tile tile)
    {
        List<Tile> neighbors = new List<Tile>();

        int x = (int)tile.GetPosition().x;
        int y = (int) tile.GetPosition().y;
        
        if (x < width - 1)
        {
            neighbors.Add(_currentMap[x + 1, y]);
            if (y < height - 1)
            {
                neighbors.Add(_currentMap[x, y + 1]);
                neighbors.Add(_currentMap[x + 1, y + 1]);
            }

            if (y > 0)
            {
                neighbors.Add(_currentMap[x, y - 1]);
                neighbors.Add(_currentMap[x + 1, y - 1]);
            }
        }

        if (x > 0)
        {
            neighbors.Add(_currentMap[x - 1, y]);
            if (y < height - 1)
            {
                neighbors.Add(_currentMap[x - 1, y + 1]);
            }

            if (y > 0)
            {
                neighbors.Add(_currentMap[x - 1, y - 1]);
            }
        }
        
        return neighbors;
    }
    
    public void GenerateMap(int smooth, int fillPercentage, string seed)
    {
        this.smooth = smooth;
        randomFillPercent = fillPercentage;
        this.seed = seed;
        GenerateMap();
    }
    
    public void GenerateMap()
    {
        _map = new int[width, height];
        _cost = new int[width, height];
        RandomFillMap();
        
        if (_currentMap != null)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Destroy(_currentMap[i, j].gameObject);
                }
            }
        }
            
        _currentMap = new Tile[width, height];
        SmoothMap();
        SmoothCost();
        InstantiateMap();
        DistributePawns();
        eventGenerator.Initialize(_currentMap);
    }


    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    _map[x, y] = 1;
                    _cost[x, y] = 5;
                }
                else
                {
                    _map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                    _cost[x, y] = pseudoRandom.Next(1, 5);
                }

            }
        }
    }
    
    void SmoothMap()
    {
        _smoothMap = _map;
        for (int i = 0; i < smooth; i++)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int neighbourWallTiles = GetSurroundingWallCount(x, y);

                    if (neighbourWallTiles > 4)
                    {
                        _smoothMap[x, y] = 1;
                    }
                    else if (neighbourWallTiles < 4)
                    {
                        _smoothMap[x, y] = 0;
                    }
                }
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += _smoothMap[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    void SmoothCost()
    {
        _smoothCost = _cost;
        for (int i = 0; i < smooth; i++)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int neighborAverageCost = GetNeighborAverageCost(x, y);

                    _smoothCost[x, y] = neighborAverageCost;

                }
            }
        }
    }

    int GetNeighborAverageCost(int gridX, int gridY)
    {
        int tileCount = 0;
        int costSum = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX < 0 || neighbourX >= width || neighbourY < 0 || neighbourY >= height) continue;

                if (neighbourX != gridX || neighbourY != gridY)
                {
                    tileCount++;
                    costSum += _cost[neighbourX, neighbourY];
                }
            }
        }

        return costSum / tileCount;
    }
    
    void InstantiateMap()
    {
        if (_map != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    GameObject t;
                    t = Instantiate(_smoothMap[x, y] == 0 ? tile : wall, new Vector3(x * tileSize.x, 0.5f, y * tileSize.y), transform.rotation);
                    t.transform.SetParent(transform);
                    _currentMap[x, y] = t.GetComponent<Tile>();
                    _currentMap[x, y].SetPosition(new Vector2(x, y));
                    _currentMap[x, y].Cost = _smoothCost[x, y];
                    //Debug.Log("Cost: " + _cost[x, y] + " Smooth: " + _smoothCost[x, y]);
                }
            }
        }
    }


    void DistributePawns()
    {
        _playableArea.Clear();
        List<Vector2Int> visited = new List<Vector2Int>();
        FloodFill(new Vector2Int(width / 2, height / 2), visited);
        int index = Random.Range(0, _playableArea.Count);
        //Debug.Log("Value: " + _playableArea[index].ToString());
        GameManager.sInstance.Rover.transform.position =
            _currentMap[_playableArea[index].x, _playableArea[index].y].transform.position;

        _playableArea.RemoveAt(index);
        for (int i = 0; i < pawns.Length; i++)
        {
            if (_playableArea.Count > 0)
            {
                for (int j = 0; j < pawns[i].quantity; j++)
                {
                    int pos = Random.Range(0, _playableArea.Count);
                    Transform pawn = Instantiate(pawns[i].prefab,
                        _currentMap[_playableArea[pos].x, _playableArea[pos].y].transform.position,
                        _currentMap[_playableArea[pos].x, _playableArea[pos].y].transform.rotation).transform;
                    pawn.position = new Vector3(pawn.position.x, pawn.position.y + 1.0f,
                        pawn.position.z);
                    _playableArea.RemoveAt(pos);
                }
            }
        }
    }

    bool WallCheck(Vector2Int position)
    {
        if (_smoothMap[position.x, position.y] == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void FloodFill(Vector2Int position, List<Vector2Int> visited)
    {
        if (!(position.x > 0 && position.x < width && position.y > 0 && position.y < height))
        {
            return;
        }

        if (visited.Contains(position))
            return;
        
        visited.Add(position);

        FloodFill(new Vector2Int(position.x - 1, position.y), visited);
        FloodFill(new Vector2Int(position.x + 1, position.y), visited);
        FloodFill(new Vector2Int(position.x, position.y - 1), visited);
        FloodFill(new Vector2Int(position.x, position.y + 1), visited);


/*        if (smoothMap[position.x, position.y] != 0) 
            return;*/
        
        if (!WallCheck(position))
        {
            _playableArea.Add(position);
        }
    }

    public Vector3 GetRandomPos()
    {
        int r = Random.Range(0, _playableArea.Count);

        Vector3 tilePos = _currentMap[_playableArea[r].x, _playableArea[r].y].transform.position;
        Vector3 randomPosition = new Vector3(tilePos.x, tilePos.y + 5.0f,
            tilePos.z);
        return randomPosition;
    }

    public Tile GetRandomWalkableTile()
    {
        int r = Random.Range(0, _playableArea.Count);
        return _currentMap[_playableArea[r].x, _playableArea[r].y];
    }
}
