﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class MapCameraController : MonoBehaviour
{
    private Camera mapCamera;
    [SerializeField] private Pathfinder pathfinder;
    private Tile start;
    private Tile end;
    [SerializeField] private LayerMask tileLayer;
    private void Awake()
    {
        mapCamera = GetComponent<Camera>();
    }

    public void Clear()
    {
        pathfinder.Clear();
    }
    
    public void HandleSelection()
    {
        RaycastHit hit;
        Ray ray = mapCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, float.MaxValue, tileLayer))
        {
            Tile tile = hit.transform.GetComponent<Tile>();
            
            if (tile)
            {
                if (!start)
                {
                    start = tile;
                }
                else
                {
                    end = tile;
                    Vector2Int startPos = new Vector2Int((int)start.GetPosition().x, (int)start.GetPosition().y);
                    Vector2Int endPos = new Vector2Int((int) end.GetPosition().x, (int) end.GetPosition().y);
                    pathfinder.FindPath(startPos, endPos);
                    start = null;
                    end = null;
                }
            }
        }
    }
}
