﻿using System;
using System.Collections;
using System.Collections.Generic;
using PetriNet_engine_csharp;
using UnityEngine;

public class Station : MonoBehaviour
{
    public enum StationType { Refuel = 0, Rearm = 1 }

    public StationType stationType;

    public void UseStation(RoverAttributes attributes)
    {
        Debug.Log("Using station");
        if ((int)stationType == 0)
        {
            attributes.fuel.AddAmount(100);
        }
        else if ((int) stationType == 1)
        {
            attributes.ammo.AddAmount(30);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Rover rover = other.transform.root.GetComponent<Rover>();
        if (rover)
        {
            Restock(rover);
        }
    }

    
    
    void Restock(Rover rover)
    {
        if (stationType == StationType.Rearm)
        {
            if (!rover.HasTokenAt((int) PlaceIndex.Rearm))
            {
                rover.AddToken((int) PlaceIndex.Rearm);
                rover.atStation = this;
            }
        }
        else
        {
            if (!rover.HasTokenAt((int) PlaceIndex.Refuel)) 
            {
                rover.AddToken((int) PlaceIndex.Refuel);
                rover.atStation = this;
            }
        }
    }
}
