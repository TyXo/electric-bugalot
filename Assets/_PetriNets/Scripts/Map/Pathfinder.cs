﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour
{

    public float clearDelay;
    
    private List<Tile> _path = new List<Tile>();
    private List<Tile> _map = new List<Tile>();
    
    public void Clear()
    {
        foreach (var tile in _map)
        {
            tile.Reset();
        }
        _map.Clear();   
        Debug.Log("Cleared all data");
        _path.Clear();
    }
    
    private void DisplayPath()
    {
        foreach (var tile in _path)
        {
            tile.Highlight();
        }

        StartCoroutine(ClearPathCoroutine());
    }

    IEnumerator ClearPathCoroutine()
    {
        yield return new WaitForSeconds(clearDelay);
        Clear();
    }
    
    public void FindPath(Vector2Int startPos, Vector2Int endPos)
    {
        Tile[,] tiles = CellularAutomata.sInstance.GetCurrentMap();
        
        Clear();
        
        foreach (var tile in tiles)
        {
            _map.Add(tile);
        }
        _path = FindShortestPath(tiles[startPos.x, startPos.y], tiles[endPos.x, endPos.y]);
        
        DisplayPath();
    }
    
    private List<Tile> FindShortestPath(Tile start, Tile end)
    {

        List<Tile> result = new List<Tile>();
        Tile node = DijkstrasAlgo(start, end);

        while (node)
        {
            result.Add(node);
            node = node.ParentNode;
        }

        result.Reverse();
        return result;
    }
    
    private Tile DijkstrasAlgo(Tile start, Tile end)
    {
        List<Tile> unexplored = new List<Tile>();

        foreach (Tile tile in _map)
        {
            if (tile.GetTileType() == TileType.walkable) ;
            {
                if (!tile.Highlighted)
                {
                    tile.Reset();
                }
                unexplored.Add(tile);
            }
        }

        start.PathCost = 0;

        while (unexplored.Count > 0)
        {
            unexplored.Sort((a, b) => (a.Cost + a.PathCost).CompareTo((b.Cost + b.PathCost)));

            Tile current = unexplored[0];
            
            if(current == end)
            {   
                return end;
            }
            
            unexplored.Remove(current);

            List<Tile> neighbours = CellularAutomata.sInstance.GetNeighboursOf(current);
            
            foreach (Tile tile in neighbours)
            {
                if (unexplored.Contains(tile) && tile.GetTileType() == TileType.walkable)
                {
                    float distance = Vector3.Distance(tile.GetPosition(), current.GetPosition());
                    distance = current.PathCost + current.Cost + distance;

                    if (distance < tile.PathCost)
                    {
                        tile.PathCost = (int) distance;
                        tile.ParentNode = current;
                    }
                }
            }
        }
        
        return end;
    }


}
