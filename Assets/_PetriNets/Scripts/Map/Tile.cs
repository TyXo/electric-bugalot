﻿using System;
using System.Collections;
using System.Collections.Generic;
using PetriNet_engine_csharp;
using UnityEngine;

public enum TileType {walkable = 0, wall = 1 }
public class Tile : MonoBehaviour
{
    [SerializeField] private TileType tileType;
    private Vector2 position;
    private int _cost;
    private int _pathCost;
    private Tile _parentNode;
    
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Material highlight;
    private Material _standardColor;
    private bool _highlighted = false;

    private Collider _collider;
    
    public bool Highlighted
    {
        get => highlight;
        set => _highlighted = value;
    }
    
    public int Cost
    {
        get => _cost;
        set => _cost = value;
    }
    
    public int PathCost
    {
        get => _pathCost;
        set
        {
            if (value < int.MaxValue)
            {
                _pathCost = value;
            }
            else
            {
                _pathCost = 99999 + Cost;
            }
        }
    }


    public Tile ParentNode
    {
        get => _parentNode;
        set => _parentNode = value;
    }

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        ParentNode = null;
        _standardColor = meshRenderer.material;
        Reset();
    }

    public void SetPosition(Vector2 position)
    {
        this.position = position;
    }

    public Vector2 GetPosition()
    {
        return position;
    }

    public TileType GetTileType()
    {
        return tileType;
    }

    public void Reset()
    {
        PathCost = int.MaxValue;
        meshRenderer.material = _standardColor;
        Highlighted = false;
        ParentNode = null;
    }

    public void Highlight()
    {
        meshRenderer.material = highlight;
        Highlighted = true;
    }

    public void Disappear(float delay)
    {
        Debug.Log("Tile at " + GetPosition() + " disappearing.");
        meshRenderer.enabled = false;
        _collider.enabled = false;
        StartCoroutine(Appear(delay));
    }

    private IEnumerator Appear(float delay)
    {
        yield return new WaitForSeconds(delay);
        meshRenderer.enabled = true;
        _collider.enabled = true;
    }

}
