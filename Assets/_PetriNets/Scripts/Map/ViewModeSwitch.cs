﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class ViewModeSwitch : MonoBehaviour
{
    [SerializeField] private Camera gameplayCamera;
    [SerializeField] private MapCameraController mapCamera;

    private bool mapOpen;

    public bool MapOpen
    {
        get => mapOpen;
    }

    private void Awake()
    {
        mapOpen = true;
        SwitchMap();
    }

    public void SwitchMap()
    {
        mapOpen = !mapOpen;
        gameplayCamera.gameObject.SetActive(!mapOpen);
        mapCamera.gameObject.SetActive(mapOpen);
    }
}
