﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager sInstance = null;
    [SerializeField]
    private Rover rover;

    public Rover Rover
    {
        get => rover;
        set => rover = value;
    }

    private void Awake()
    {
        if (sInstance)
        {
            Destroy(this);
        }
        else
        {
            sInstance = this;
        }
    }
    

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void Close()
    {
        Application.Quit();
    }
}
