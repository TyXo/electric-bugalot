﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class SliderTextUpdater : MonoBehaviour
{
    [SerializeField] private Slider m_Slider;

    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Start()
    {
        UpdateText();
    }

    public void UpdateText()
    {
        text.text = m_Slider.value.ToString();
    }
}
