﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsSetter : MonoBehaviour
{
    [SerializeField] private Slider m_Smooth;
    [SerializeField] private Slider m_FillPercentage;
    [SerializeField] private InputField m_Seed;
    public void SetSmooth()
    {
        CellularAutomata.sInstance.SetSmooth((int)m_Smooth.value);
    }

    public void SetFillPercentage()
    {
        CellularAutomata.sInstance.SetFillPercentage((int)m_FillPercentage.value);
    }

    public void SetSeed()
    {
        CellularAutomata.sInstance.SetSeed(m_Seed.text);
    }

    public void SetRandomSeedUsage(bool useRandomSeed)
    {
        CellularAutomata.sInstance.SetRandomSeed(useRandomSeed);
    }
}
