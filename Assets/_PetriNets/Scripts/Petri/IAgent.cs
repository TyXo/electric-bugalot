﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAgent
{
    void Cycle(int place);
}
