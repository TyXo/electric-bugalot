﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetriNet_engine_csharp
{
    class Place
    {
        private int _iD;
        private Boolean empty;
        private List<Token> tokensList;
    
        public Place(int id)
        {
            this._iD = id;
            this.empty = true;
            tokensList = new List<Token>();
        }
        public void addToken(Token token)
        {
            tokensList.Add(token);
            empty = false;
        }

        public void ConsumeToken(Token token)
        {
            if (tokensList.Contains(token))
            {
                int pos = tokensList.IndexOf(token);
                tokensList.Remove(tokensList[pos]);
            }
        }

        public Token GetFirstToken()
        {
            if (tokensList.Count > 0)
            {
                return tokensList[0];
            }
            else
            {
                return null;
            }
        }

        public Boolean isEmpty()
        {
            return empty;
        }
        public int getId()
        {
            return _iD;
        }
    }
}
