﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace PetriNet_engine_csharp
{
    class PetriNet
    {
        private List<Place> placeList = new List<Place>();
        private List<Transition> transitionList = new List<Transition>();
        public PetriNet()
        {
            
        }

        public void execCycle(IAgent agent)
        {
            for (int i = 0; i < placeList.Count; i++)
            {
                if (!placeList[i].isEmpty())
                {
                    agent.Cycle(i);
                }
            }
        }
        
        public void insertTransition(Transition transition)
        {
            transitionList.Add(transition);
        }
        
        public void insertPlace(Place place)
        {
            placeList.Add(place);
        }
        
        public Transition getTransition(int pos)
        {
            return transitionList[pos];
        }
        
        public Place getPlace(int pos)
        {
            return placeList[pos];
        }

    }
}
