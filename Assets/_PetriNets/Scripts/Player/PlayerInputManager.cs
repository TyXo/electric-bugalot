﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputManager : MonoBehaviour
{

    [SerializeField] private ViewModeSwitch viewModeSwitch;
    [SerializeField] private MapCameraController mapCameraController;
    private void Update()
    {
        if (Input.GetButtonDown("OpenMap"))
        {
            SwitchMap();
        }
        
        if (Input.GetButtonDown("Fire1") && viewModeSwitch.MapOpen)
        {
            mapCameraController.HandleSelection();
        }
        
        if (Input.GetButtonDown("Fire2") && viewModeSwitch.MapOpen)
        {
            mapCameraController.Clear();
        }
    }

    private void SwitchMap()
    {
        Debug.Log("Opening map");
        viewModeSwitch.SwitchMap();
    }

}
