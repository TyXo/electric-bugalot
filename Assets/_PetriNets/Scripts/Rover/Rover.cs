﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PetriNet_engine_csharp;
using UnityEngine.Serialization;


public enum PlaceIndex { Move = 0, Rearm = 1, Refuel = 2, Rescue = 3, Attack = 4, Defend = 5}
public class Rover : MonoBehaviour, IAgent
{
    private PetriNet roverNet;
    private RoverAttributes attributes;

    public Station atStation
    {
        get;
        set;
    }
    
    private void Start()
    {
        attributes = new RoverAttributes(50, 30, 100);
        BuildRoverNet();
    }
    
    void BuildRoverNet()
    {
        roverNet = new PetriNet();
        roverNet.insertPlace(new Place((int)PlaceIndex.Move));
        roverNet.getPlace((int)PlaceIndex.Move).addToken(new Token((int)PlaceIndex.Move));
        roverNet.insertTransition(new Transition((int)PlaceIndex.Move));
        roverNet.getTransition((int) PlaceIndex.Move)
            .insertConnection(new Connection(roverNet.getPlace((int) PlaceIndex.Move), true, false));
        
        roverNet.insertPlace(new Place((int)PlaceIndex.Rearm));
        roverNet.insertPlace(new Place((int)PlaceIndex.Refuel));
        roverNet.insertPlace(new Place((int)PlaceIndex.Rescue));
        roverNet.insertPlace(new Place((int)PlaceIndex.Attack));
        roverNet.insertPlace(new Place((int)PlaceIndex.Defend));
    }

    private void Update()
    {
        roverNet.execCycle(this);
    }

    public void AddToken(int place)
    {
        roverNet.getPlace(place).addToken(new Token(0));
    }

    public RoverAttributes Restock()
    {
        return attributes;
    }

    public bool HasTokenAt(int place)
    {
        return !roverNet.getPlace(place).isEmpty();
    }

    public void Cycle(int place)
    {
        if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Move)
        {
            
        }
        else if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Rearm)
        {
            if (atStation && atStation.stationType == Station.StationType.Rearm)
            {
                atStation.UseStation(attributes);
                atStation = null;
                roverNet.getPlace(place).ConsumeToken(roverNet.getPlace(place).GetFirstToken());
            }
        }
        else if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Refuel)
        {
            if (atStation && atStation.stationType == Station.StationType.Refuel)
            {
                atStation.UseStation(attributes);
                atStation = null;
                roverNet.getPlace(place).ConsumeToken(roverNet.getPlace(place).GetFirstToken());
            }   
        }
        else if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Rescue)
        {
            
        }
        else if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Attack)
        {
            
        }
        else if (roverNet.getPlace(place).getId() == (int)PlaceIndex.Defend)
        {
            
        }
    }
    
}
