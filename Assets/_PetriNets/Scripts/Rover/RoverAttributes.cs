﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RoverAttributes
{
    public Attribute health;
    public Attribute ammo;
    public Attribute fuel;

    public RoverAttributes(int maxHealth, int maxAmmo, int maxFuel)
    {
        health = new Attribute(maxHealth);
        ammo = new Attribute(maxAmmo);
        fuel = new Attribute(maxFuel);
    }
}

[Serializable]
public class Attribute
{
    [SerializeField] private int _amount;
    [SerializeField] private int _maxAmount;

    public Attribute(int maxAmount)
    {
        SetMaxAmount(maxAmount);
    }
    
    public int GetAmount()
    {
        return _amount;
    }

    public void SetMaxAmount(int maxAmount)
    {
        _maxAmount = maxAmount;
        _amount = _maxAmount;
    }

    public void AddAmount(int amount)
    {
        _amount += amount;
        if (_amount > _maxAmount)
        {
            _amount = _maxAmount;
        }
    }
}