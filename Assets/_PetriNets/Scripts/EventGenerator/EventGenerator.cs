﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventGenerator : MonoBehaviour
{
    [SerializeField] private EventData eventData;
    private EventData _currentEvent;
    private List<Tile> _map = new List<Tile>();
    
    public void Initialize(Tile[,] map)
    {
        _currentEvent = ScriptableObject.Instantiate(eventData);
        foreach (var tile in map)
        {
            _map.Add(tile);
        }

        EventTick();
    }

    private void EventTick()
    {
        StartCoroutine(DelayCoroutine());
    }

    IEnumerator DelayCoroutine()
    {
        yield return new WaitForSeconds(_currentEvent.delay);
        ProbabilityRandomizer.ProbableValue[] probability = 
        {
            new ProbabilityRandomizer.ProbableValue(0, 0, 1 - _currentEvent.probability),
            new ProbabilityRandomizer.ProbableValue(1, 1, _currentEvent.probability)
        };

        if(ProbabilityRandomizer.GetRandom(probability) > 0)
        {
            Debug.Log("Playing Event");
            PlayEvent();
        }
        else
        {
            Debug.Log("Not playing event;");
        }
        EventTick();
    }

    private void PlayEvent()
    {
        Tile tile = CellularAutomata.sInstance.GetRandomWalkableTile();
        _currentEvent.PlayEvent(tile);
    }
}
