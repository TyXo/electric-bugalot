using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ProbabilityRandomizer
{
    public struct ProbableValue
    {
        private int _minValue;
        private int _maxValue;
        public float probability;

        public ProbableValue(int minValue, int maxValue, float probability)
        {
            _minValue = minValue;
            _maxValue = maxValue;
            this.probability = probability;
        }

        public int GetValue()
        {
            return Random.Range(_minValue, _maxValue + 1);
        }
    }
        
        
    public static int GetRandom(ProbableValue[] probableValues)
    {

        float rand = Random.value;
        float currentProbability = 0;
        foreach (var probableValue in probableValues)
        {
            currentProbability += probableValue.probability;
            if (rand <= currentProbability)
            {
                return probableValue.GetValue();
            }
        }
            
        return -1;
    }
}
