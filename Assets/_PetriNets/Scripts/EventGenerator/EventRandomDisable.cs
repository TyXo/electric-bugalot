﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName =  "Event Data", menuName = "Event Generator/Random Tile Disabler")]
public class EventRandomDisable : EventData
{
    public override void PlayEvent(Tile tile)
    {
        base.PlayEvent(tile);
        tile.Disappear(duration);
    }
}
