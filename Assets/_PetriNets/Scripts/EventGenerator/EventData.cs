﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventData : ScriptableObject
{
    public float delay = 40;
    public float duration;
    [Range(0, 1)] public float probability;
    public virtual void PlayEvent(Tile tile)
    {
    }
}
