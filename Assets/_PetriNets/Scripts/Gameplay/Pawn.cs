﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Pawn")]
public class Pawn : ScriptableObject
{
    public GameObject prefab;
    public int quantity;
}
