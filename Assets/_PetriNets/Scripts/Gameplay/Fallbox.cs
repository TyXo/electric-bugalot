﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fallbox : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Rover rover = other.transform.root.GetComponent<Rover>();
     
        if (rover)
        {
            TeleportRover(rover);
        }
    }

    private void TeleportRover(Rover rover)
    {
        Vector3 pos = CellularAutomata.sInstance.GetRandomPos();
        rover.transform.position = pos;
    }
    
}
